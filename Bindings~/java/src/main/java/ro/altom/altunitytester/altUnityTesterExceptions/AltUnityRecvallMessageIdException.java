package ro.altom.altunitytester.altUnityTesterExceptions;

public class AltUnityRecvallMessageIdException extends AltUnityRecvallException {

    /**
     *
     */
    private static final long serialVersionUID = 4931599539554605123L;

    public AltUnityRecvallMessageIdException() {
    }

    public AltUnityRecvallMessageIdException(String message) {
        super(message);
    }

}
