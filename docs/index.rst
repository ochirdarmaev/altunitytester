.. AltUnity Tester documentation master file, created by
   sphinx-quickstart on Thu Oct 17 09:19:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

AltUnity Tools
##############


Through AltUnity Tools, we provide a UI Test Automation solution for Unity games.

 - AltUnity Tester is a free, open source asset. Its main goal is to enable UI test automation, by instrumenting games to get access and programmatically control the Unity objects. Get AltUnity Tester from `Unity Asset Store <https://assetstore.unity.com/packages/tools/utilities/altunity-tester-ui-test-automation-112101>`_.

 - AltUnity Inspector is a desktop application allowing users to inspect the object hierarchy and interact with their app outside the Unity Editor. Get AltUnity Inspector from `our website <https://altom.com/testing-tools/altunitytester/>`_.


Documentation
=============

 .. rst-class:: toc

*  `AltUnity Tester <https://altom.gitlab.io/altunity/altunitytester/tester.html>`_ 

   *  `Overview <https://altom.gitlab.io/altunity/altunitytester/pages/overview.html>`_ 
   *  `Get Started <https://altom.gitlab.io/altunity/altunitytester/pages/get-started.html>`_ 

*  `AltUnity Inspector <https://altom.gitlab.io/altunity/altunityinspector/inspector.html>`_ 

   *  `Overview <https://altom.gitlab.io/altunity/altunityinspector/pages/overview.html>`_ 
   *  `Get Started <https://altom.gitlab.io/altunity/altunityinspector/pages/get-started.html>`_ 




.. toctree::
   :caption: Table of contents:
   :hidden:

   tester
   AltUnity Inspector <https://altom.gitlab.io/altunity/altunityinspector/inspector.html>
   

.. toctree::
   :caption: Community:
   :hidden:

    Discord <https://discord.gg/Ag9RSuS>
    Google Group <https://groups.google.com/a/altom.com/forum/#!forum/altunityforum>
